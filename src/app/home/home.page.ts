import { Component } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: [ 'home.page.scss' ],
})
export class HomePage {
    currentRecordedFile: MediaObject;
    lastRecordedFile: MediaObject;
    recordedPaths: string[] = [];
    isPlaying = false;

    public constructor(
        private file: File,
        private media: Media,
    ) {
    }

    public record(): void {
        const fileName = this.getFileName();
        this.file.createFile(this.file.tempDirectory, fileName, true)
            .then(() => {
                const filePath = `${this.file.tempDirectory}${fileName}`.replace(/^file:\/\//, '');
                this.currentRecordedFile = this.media.create(filePath);
                this.currentRecordedFile.startRecord();
                this.recordedPaths.push(filePath);
            })
            .catch((cause) => {
                console.log('Could not create file', cause);
            });
    }

    private getFileName(): string {
        const id = this.recordedPaths.length + 1;
        return `record_${id}.m4a`;
    }

    public stopRecording(): void {
        this.currentRecordedFile.stopRecord();
        this.lastRecordedFile = this.currentRecordedFile;
        this.currentRecordedFile = null;

    }

    public play(): void {
        this.lastRecordedFile.play();
        this.isPlaying = true;
    }

    public stop(): void {
        this.lastRecordedFile.stop();
        this.isPlaying = false;
    }
}
